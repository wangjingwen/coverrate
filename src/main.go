package main

import (
	"fmt"
	"net/http"
	_ "net/http"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) { fmt.Fprintf(w, "I'm a cook server.") })
	http.ListenAndServe(":3001", mux)

	fmt.Printf("Hello, world.\n")
}
