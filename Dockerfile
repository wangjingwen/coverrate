FROM docker-reg.sogou-inc.com/official/golang:1.14.0
WORKDIR /go/src/gitlab.com/wangjingwen/coverrate

COPY /sys ./sys
COPY /src ./src

RUN chmod 777 sys/goc
WORKDIR /go/src/gitlab.com/wangjingwen/coverrate/src
RUN ./../sys/goc build -o /go/bin/cover --center="http://goc.thanos-lab.sogou" --agentport=":5678"
WORKDIR /go/src/gitlab.com/wangjingwen/coverrate
#FROM docker-reg.sogou-inc.com/ime_server/alpine:latest

#COPY --from=build /go/bin /go/bin
#COPY --from=build /go/src/coverRate /go/src/coverRate

EXPOSE 80

COPY ./sys/run.sh /etc/run.sh
RUN chmod 766 /etc/run.sh
CMD ["/bin/bash","-c","/etc/run.sh"]